import System.IO
import System.Random
import Control.Arrow

-- Comments: https://gist.github.com/joom/7d220e603d58ff889902 was helpful in showing what needed to be done and offer help if I was stuck.

--Comments: Own typeclass called Move containing the moves. Bounded derive is used for the minBound, MaxBound class methods.
data Move = Rock | Paper | Scissors
           deriving (Show, Read, Enum, Bounded)

main :: IO ()
main = do
  putStr "Your move? [Rock, Paper, or Scissors]: "
  m1 <- getLine >>= readIO
  m2 <- randomIO
  putStrLn $ show m1 ++ " vs. " ++ show m2
  putStrLn $ play m1 m2
  main

play :: Move -> Move -> String
play m1 m2
  | m1 `beats` m2 = "You win!"
  | m2 `beats` m1 = "You lose."
  | otherwise = "Tie."

--Function: Takes in two moves (player,computer) and returns a boolean
--Comments: Setting if a move 'beats' another move. 
-- Help from http://codereview.stackexchange.com/a/88793
beats :: Move -> Move -> Bool
Rock     `beats` Scissors = True
Paper    `beats` Rock     = True
Scissors `beats` Paper    = True
_        `beats` _        = False

--Comments: Help with the random from https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/randoms specifically the 'Coin Flip'
-- monte carlo method
instance Random Move where
  random = randomR (minBound, maxBound)
  randomR (a, b) g = first toEnum $ randomR (a', b') g
    where a' = fromEnum a
          b' = fromEnum b



